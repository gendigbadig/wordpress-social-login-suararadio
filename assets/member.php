<?php
/*
Template Name: Member Index1
*/
global $suararadio_connect;
if (!is_user_logged_in()) {
		#$uname = $current_user->user_nicename;	
		#var_dump($current_user);
		wp_redirect(get_option('siteurl') . '/login/');
		exit;
}


if (is_user_logged_in()) {
	//$current_user->fbuid != 
	#var_dump($_COOKIE);
	#var_dump($_SESSION);
}

$DS = DIRECTORY_SEPARATOR;

$inc_playlist = SUARARADIO_PLUGIN_DIR.'/includes/member_playlist.php';
if (file_exists(TEMPLATEPATH.$DS.'member_playlist.php')) {
	$inc_playlist = TEMPLATEPATH.$DS.'member_playlist.php';
}

$inc_account_view = SUARARADIO_PLUGIN_DIR.'/includes/member_account_view.php';
if (file_exists(TEMPLATEPATH.$DS.'member_account_view.php')) {
	$inc_account_view = TEMPLATEPATH.$DS.'member_account_view.php';
}

	define('SUARARADIO_PLAYER_SHOWED',true);
	
	#$uname = "-none-";	
	$uname = $current_user->user_nicename;	
	$requested = $suararadio->getBaseReq();
	$urlpath = explode('/',$requested);
	$rootpath = $urlpath[1];
	
	#$morder = ($_POST['morder']) ? $_POST['morder']: (($_SESSION['member']['morder'])? $_SESSION['member']['morder']:'newest');
	#$mcat = ($_POST['mcat']) ? $_POST['mcat']: (($_SESSION['member']['mcat'])? $_SESSION['member']['mcat']:'');
	#$mkeyword = $_POST['mkeyword'];
	$filter = $suararadio->getStatData();
	
	#global $id;
	$pagemember = get_page_by_path( '/'.$rootpath);
	$args = array('child_of'=>$pagemember->ID,'sort_column'=>'menu_order','exclude'=>'1552'); #,'exclude'=>'1552'
	$children = get_pages($args);
	$foundpath = false;
	#var_dump($children);
	#var_dump($rootpath,$requested,$foundpath);
	
	$arpath = array(
		"/member/"=>"\/member\/",
		"/member/music/"=>"\/member\/music\/",
		"/member/account/"=>"\/member\/account\/",
		"/member/payment/"=>"\/member\/payment\/");
	foreach ($arpath as $vkey=>$vpath) {
		if (preg_match("/^".$vpath."$/",$requested,$match) || 
			preg_match("/^".$vpath."page/",$requested,$match)) {
				$foundpath = $vkey;
			}
	}
	if (preg_match('/^\/member\/music\/(\d+)/',$requested,$matches)) {
		$foundpath = $requested;
	}
	
if ($_REQUEST['load']!='dialog') get_header();		
if (preg_match('/^\/member\/music\/(\d+)/',$foundpath,$matches)) {
	$songId = $matches[1];
	$inc_info = SUARARADIO_PLUGIN_DIR.'/includes/member_music_info.php';
	if (file_exists(TEMPLATEPATH.$DS.'member_music_info.php')) {
		$inc_info = TEMPLATEPATH.$DS.'member_music_info.php';
	}
	include $inc_info;
} else if ($foundpath=='/member/account/') {
	$inc_account = SUARARADIO_PLUGIN_DIR.'/includes/member_account.php';
	if (file_exists(TEMPLATEPATH.$DS.'member_account.php')) {
		$inc_account = TEMPLATEPATH.$DS.'member_account.php';
	}
	$action = $_REQUEST['action'];
	switch ($action) {
		case "upgrade":
			$inc_account = SUARARADIO_PLUGIN_DIR.$DS.'includes'.$DS.'member_account_upgrade.php';
			break;
	}
	include $inc_account;
} else {
	$inc_front = SUARARADIO_PLUGIN_DIR.'/includes/member_front.php';
	if (file_exists(TEMPLATEPATH.$DS.'member_front.php')) {
		$inc_front = TEMPLATEPATH.$DS.'member_front.php';
	}
	include $inc_front;	
}
if ($_REQUEST['load']!='dialog') get_footer();
<?php
/*
Template Name: Login Membership
*/

wp_enqueue_style( "wsl_login_css", WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL . "/assets/css/login.css" );
?>
<?php get_header(); ?>
<div class="loginContainer">
<div class="loginBgOuter">
	<section class="loginTop">
		<article class="loginFrm2">
		<?php wsl_render_login_form(); ?>
		<section style="width: 50%; margin: auto;">
		</section>
		</article>
	</section>
	<section class="loginBottom">
		<div class="loginBottomBg">
			<aside class="logInfo listener">
			<header>Radio Listener</header>
			<ul>
				<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/li_1.png"/><p>Get Discounts on All of The MODIS Outlets around Indonesia.</p></li>
				<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/li_2.png"/><p>Unlimited Radio Station Content Streaming</p></li>
				<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/li_3.png"/><p>Music Preview Provided by Melon</p></li>
				<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/li_4.png"/><p>Free Advertising for one product/service on LARIS for a day per month</p></li>
			</ul>
			<footer style="right: 40px;"><!--   img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/i_list.png"/ --></footer>
		</aside>
	<aside class="logInfo fans">
			<header>Radio Fans</header>
			<ul>
				<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/fa_1.png"/><p>All The Features of Radio Listener</p></li>
				<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/fa_2.png"/><p>Unlimited Music Streaming Provided by MelOn</p></li>
				<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/fa_3.png"/><p>Free Advertising for one product/service on LARIS for 7 days per month</p></li>
			</ul>
			<footer>&nbsp;</footer>
		</aside>
		<aside class="logInfo lover">
			<header>Radio Lover</header>
			<ul>
				<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/lo_1.png"/><p>All The Features of Radio Fans</p></li>
				<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/lo_2.png"/><p>Unlimited Radio Collaboration  Content</p></li>
				<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/lo_3.png"/><p>Free Advertising for one product/service on LARIS for 14 days per month</p></li>
			</ul>
			<footer>&nbsp;</footer>
		</aside>
	
		<aside class="logInfo legend">
			<header>Radio Legend</header>
			<ul>
				<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/lg_1.png"/><p>All The Features of Radio Mania</p></li>
				<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/lg_2.png"/><p>Realtime Monitoring Facility for: Rundown, Request, Radio & Listener profile</p></li>
				<li><img src="<?php echo WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL ?>/assets/img/info/lg_3.png"/><p>Free Advertising for one product/service on LARIS for 30 days per month</p></li>
			</ul>
			<footer>&nbsp;</footer>
		</aside>
		</div>
	</section>
</div>
</div>
<?php get_footer(); ?>

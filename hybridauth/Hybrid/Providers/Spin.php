<?php
// credit to: http://hp.nonip.info/wiki/work/index.php?php%2FHybridAuth
class Hybrid_Providers_Spin extends Hybrid_Provider_Model_OpenID
{
	var $openidIdentifier = "https://api.suararadio.com/spin/user/openid";
	
	function loginFinish()
	{
		# if user don't garant acess of their data to your site, halt with an Exception
		if( $this->api->mode == 'cancel'){
			throw new Exception( "Authentication failed! User has canceled authentication!", 5 );
		}

		# if something goes wrong
		if( ! $this->api->validate() ){
			throw new Exception( "Authentication failed. Invalid request recived!", 5 );
		}

		# fetch recived user data
		$response = $this->api->getAttributes();

		# sotre the user profile
		$this->user->profile->identifier  = (array_key_exists("person/guid",$response))?$response["person/guid"]:"";

		$this->user->profile->firstName   = (array_key_exists("namePerson/first",$response))?$response["namePerson/first"]:"";
		$this->user->profile->lastName    = (array_key_exists("namePerson/last",$response))?$response["namePerson/last"]:"";
		$this->user->profile->displayName = (array_key_exists("namePerson",$response))?$response["namePerson"]:"";
		$this->user->profile->email       = (array_key_exists("contact/email",$response))?$response["contact/email"]:"";
		$this->user->profile->language    = (array_key_exists("pref/language",$response))?$response["pref/language"]:"";
		$this->user->profile->country     = (array_key_exists("contact/country/home",$response))?$response["contact/country/home"]:""; 
		$this->user->profile->zip         = (array_key_exists("contact/postalCode/home",$response))?$response["contact/postalCode/home"]:""; 
		$this->user->profile->gender      = (array_key_exists("person/gender",$response))?$response["person/gender"]:""; 
		$this->user->profile->photoURL    = (array_key_exists("media/image/default",$response))?$response["media/image/default"]:""; 

		$this->user->profile->birthDay    = (array_key_exists("birthDate/birthDay",$response))?$response["birthDate/birthDay"]:""; 
		$this->user->profile->birthMonth  = (array_key_exists("birthDate/birthMonth",$response))?$response["birthDate/birthMonth"]:""; 
		$this->user->profile->birthYear   = (array_key_exists("birthDate/birthDate",$response))?$response["birthDate/birthDate"]:"";  

		if( ! $this->user->profile->displayName ) {
			$this->user->profile->displayName = trim( $this->user->profile->lastName . " " . $this->user->profile->firstName ); 
		}

		if( isset( $response['namePerson/friendly'] ) && ! empty( $response['namePerson/friendly'] ) && ! $this->user->profile->displayName ) { 
			$this->user->profile->displayName = (array_key_exists("namePerson/friendly",$response))?$response["namePerson/friendly"]:"" ; 
		}

		if( isset( $response['birthDate'] ) && ! empty( $response['birthDate'] ) && ! $this->user->profile->birthDay ) {
			list( $birthday_year, $birthday_month, $birthday_day ) = (array_key_exists('birthDate',$response))?$response['birthDate']:"";

			$this->user->profile->birthDay      = (int) $birthday_day;
			$this->user->profile->birthMonth    = (int) $birthday_month;
			$this->user->profile->birthYear     = (int) $birthday_year;
		}

		if( ! $this->user->profile->displayName ){
			$this->user->profile->displayName = trim( $this->user->profile->firstName . " " . $this->user->profile->lastName );
		}
		if( $this->user->profile->gender == "f" ){
			$this->user->profile->gender = "female";
		}
		if( $this->user->profile->gender == "m" ){
			$this->user->profile->gender = "male";
		}
		$this->user->profile->description = (array_key_exists("description",$response))?$response["description"]:"";
		if (!$this->user->profile->email) {
			$this->user->profile->email = strtolower( $this->providerId . "_" . $this->user->profile->identifier ) . "@klubradio.co.id";
		}

		// set user as logged in
		$this->setUserConnected();

		// with openid providers we get the user profile only once, so store it 
		Hybrid_Auth::storage()->set( "hauth_session.{$this->providerId}.user", $this->user );
	} //endfunc finish
}
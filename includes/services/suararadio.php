<?php
function suararadio_fb_init_js() {
    global $suararadio_connect;
    
    $wsl_concat = (defined('WSL_CONCAT')) ? WSL_CONCAT:'';
    
    if (!get_option('wsl_settings_Facebook'.$wsl_concat.'_enabled')) return false;
    
    #echo '<div id="data"></div>'."\n";
    #echo '<div id="fb-root"></div>'."\n";
    #echo '<div id="log"></div>'."\n";

    echo '<script src="http://connect.facebook.net/en_US/all.js"></script>'."\n";
    
    $appId = get_option( "wsl_settings_Facebook".$wsl_concat."_app_id" );
    echo "<script>
        FB.init({ 
            appId:'".$appId."', ";
            /*if ($suararadio_connect['facebook']['session']) {
             echo "
             session : ".json_encode($suararadio_connect['facebook']['session']).", // don't refetch the session when PHP already has it ";
             }*/
    echo " 
            cookie:true, 
            status:true, 
            xfbml:true 
            });
    
        function streamPublish(message, name, description, hrefTitle, hrefLink, nmFileFb, urlUFb, mediaUFb, urlSite, pluginURL, uidfb, postId, imageLink){
        FB.ui(
              {
              /*method: 'feed', 
              display: 'popup',
              link: hrefLink,
              
              source: 'https://api.suararadio.com/assets/player.swf?file='+nmFileFb+'&autostart=true&image=https://api.suararadio.com/stayTuned/image&stretching=exactfit&logo.hide=false&logo.file=https://api.suararadio.com/stayTuned/logo', 
              type:(mediaUFb=='mp3')?'audio/mpeg':mediaUFb,
              picture:((typeof imageLink === 'undefined')?'https://api.suararadio.com/assets/smaller.jpg':imageLink),
              message: message,
              name : name,
              description : description,
              
              
              
              
              action_links: [
              { text: hrefTitle, href: hrefLink }
              ],*/
	      method: 'share',
              href: hrefLink,
              auto_publish:'true'
              },
              function(response) {
              /*if (response == 0){
              alert('Your facebook status not updated. Give Status Update Permission.');
              }
              else{                     
              var uidfb1 = FB.getAuthResponse().userID;
              var url	= '".WP_PLUGIN_URL."'+'/suararadio/includes/processFbLog.php';
              url		= url+'?postid='+postId+'&uidfb='+uidfb1+'&history='+message+'&postidstream='+response.post_id;
              ajaxLoader(url,'contentLYR');
              }*/

		if(!response) return;
              var uidfb1 = FB.getAuthResponse().userID;
              var url	= '".WP_PLUGIN_URL."'+'/suararadio/includes/processFbLog.php';
              url		= url+'?postid='+postId+'&uidfb='+uidfb1+'&history='+message+'&postidstream='+response.post_id;
	      ajaxLoader(url,'contentLYR'); 
		
              });
        
    }
    
    function streamPublish_streaming(message, name, description, hrefTitle, hrefLink, nmFileFb, urlUFb, mediaUFb, urlSite, pluginURL, uidfb, postId, imageLink, stat){
        if(stat =='aac'){
            stat_stream = '.flv';
        }else{
            stat_stream = '.mp3';
        }
       
        FB.ui(
              {
              method: 'feed', 
              display: 'popup',
              link: hrefLink,
              
              source: 'https://api.suararadio.com/player/player_stream.swf?file='+nmFileFb+'?type='+stat_stream+'&autostart=true&image='+imageLink+'&logo.hide=false&logo.file=https://api.suararadio.com/stayTuned/logo&backcolor=0xe6e6e6', 
              type:(mediaUFb=='mp3')?'audio/mpeg':mediaUFb,
              picture:((typeof imageLink === 'undefined')?'https://api.suararadio.com/assets/smaller.jpg':imageLink),
              message: message,
              name : name,
              description : description,
              action_links: [
              { text: hrefTitle, href: hrefLink }
              ],
              auto_publish:'true'
              },
              function(response) {
              if (response == 0){
              alert('Your facebook status not updated. Give Status Update Permission.');
              }
              else{                     
              var uidfb1 = FB.getAuthResponse().userID;
             
              var url	= '".WP_PLUGIN_URL."'+'/suararadio/includes/processFbLog.php';
              url		= url+'?postid='+postId+'&uidfb='+uidfb1+'&history='+message+'&postidstream='+response.post_id;
              ajaxLoader(url,'contentLYR');
              }
              });
        
    }
              		
    function postToFeed(stay, postId, link_api, uidfb){
        FB.ui({
              method: 'feed',
              link: link_api,
              display: 'popup',
              auto_publish:'true'
              },
              function(response) {
              	if (response == 0){
              		alert('Your facebook status not updated. Give Status Update Permission.');
              	} else{
              		var uidfb1 = FB.getAuthResponse().userID;
              		var url   = '".WP_PLUGIN_URL."/suararadio/includes/processFbLog.php';
              		url  = url+'?postid='+postId+'&uidfb='+uidfb1+'&history='+message+'&postidstream='+response.post_id;
              		ajaxLoader(url,'contentLYR');
              	}
		});

    }
    
      function streamPublish_melon(stay, postId, link_api, uidfb){
        FB.ui({
              method: 'feed',
              link: link_api,
              display: 'popup'
              },
              function(response) {
              	if (response == 0){
              		alert('Your facebook status not updated. Give Status Update Permission.');
              	} else{
              		var uidfb1 = FB.getAuthResponse().userID;
              		var url   = '".WP_PLUGIN_URL."/suararadio/includes/processFbLog.php';
              		url  = url+'?postid='+postId+'&uidfb='+uidfb1+'&history='+message+'&postidstream='+response.post_id;
              		ajaxLoader(url,'contentLYR');
              	}
		});

    }
    
    function ajaxLoader(url,id)
    {
        try{
            // Opera 8.0+, Firefox, Safari
            xmlHttp = new XMLHttpRequest();
        } catch (e){
            // Internet Explorer Browsers
            try{
                xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
            } catch (e) {
                try{
                    xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
                } catch (e){
                    // Ajax is not supported
                    alert('Your browser does not support Ajax');
                    return false;
                }
            }
        }
        
        xmlHttp.onreadystatechange = function()
        {
            if (xmlHttp.readyState == 4) {
                document.getElementById('results').innerHTML= xmlHttp.responseText;
            }
        }
        xmlHttp.open('GET', url, true);
        xmlHttp.send(null);
    }
    </script>";
} //endfunc suararadio_fb_init_js

function loginFBCustom(){
    $out = "
	<script type='text/javascript'>				
    function cekFB(){
        var dialog = {
            method: 'permissions.request',
            perms: 'publish_actions, read_stream'
        };  
        
        FB.ui(dialog,null);
    }
    </script>	";
	echo $out;
}
    
function suararadio_set_init() {
        global $suararadio_connect;
        
        if (!$suararadio_connect) {
            require_once( ABSPATH . '/wp-load.php' );
            require_once( WORDPRESS_SOCIAL_LOGIN_ABS_PATH . "/hybridauth/Hybrid/Auth.php" );
            $wsl_concat = (defined('WSL_CONCAT')) ? WSL_CONCAT:'';
            
            $appId = get_option( "wsl_settings_Facebook".$wsl_concat."_app_id" );
            $secret = get_option( "wsl_settings_Facebook".$wsl_concat."_app_secret" );
            
            if (get_option('wsl_settings_Facebook'.$wsl_concat.'_enabled')) {
                $suararadio_connect['options']['facebook'] = array(
                    'appId'=>$appId,
                    'secret'=>$secret,
                    'active'=>'1',
                    'transparent'=>'1',
                    'autologout'=>'1',
                );
                $suararadio_connect['facebook'] = null;
                
                include_once(WORDPRESS_SOCIAL_LOGIN_ABS_PATH.'/hybridauth/Hybrid/thirdparty/Facebook/facebook.php');
                
                $facebook = new Facebook(array(
                       			'appId'  => $suararadio_connect['options']['facebook']['appId'],
                                'secret' => $suararadio_connect['options']['facebook']['secret'],
                                'cookie' => true,
                            ));
                $fbsession = $facebook->getUser();			
                $suararadio_connect['facebook']['obj'] = &$facebook;
                $suararadio_connect['facebook']['session']['uid'] = $fbsession;
 /*               
                $config = array();
                $config["base_url"]  = WORDPRESS_SOCIAL_LOGIN_PLUGIN_URL . '/hybridauth/';
                $config["providers"] = array();
                $config["providers"]['Facebook'] = array();
                $config["providers"]['Facebook']["enabled"] = true;
                $config["providers"]['Facebook']["keys"]["id"] = $suararadio_connect['options']['facebook']['appId'];
                $config["providers"]['Facebook']["keys"]["secret"] = $suararadio_connect['options']['facebook']['secret'];
                $config["debug_mode"] = true;
                $config["debug_file"] = WORDPRESS_SOCIAL_LOGIN_DIR."/debug.txt";
                
                $hybridauth = new Hybrid_Auth( $config );
                if ($_SERVER['REMOTE_ADDR']=='118.96.201.78') {
                    #var_dump($hybridauth);
                    var_dump($hybridauth->isConnectedWith('Facebook'));
                }
                
                if( $hybridauth->isConnectedWith('Facebook')) {
                    #echo "xxxxx";
                    $adapter = $hybridauth->getAdapter( 'Facebook' );
                    $suararadio_connect['facebook']['obj'] = $adapter;
                    $user_profile = $adapter->getUserProfile();
                    if ($_SERVER['REMOTE_ADDR']=='118.96.201.78') {
                        var_dump($user_profile);
                    }
                    //$suararadio_connect['facebook']['session']['uid'] = 
                }
*/
            }
/*
            $suararadio_connect['options'] = get_option('suararadio_connect');
            if ($_SERVER['SERVER_NAME']=='www.diradio.net') {
                $suararadio_connect['options']['facebook'] = array(
                                                                   'appId'=>'133985053149',
                                                                   'secret'=>'66b313f01440c8826dea12b275e6be0f',
                                                                   'active'=>'1',
                                                                   'transparent'=>'1',
                                                                   'autologout'=>'1',
                                                                   'register_url'=>'http://www.diradio.net/wp-login.php?action=register',
                                                                   );
            }
            
            $options = &$suararadio_connect['options'];
            
            /// tambahan untuk
            
            // init //
            $suararadio_connect['facebook'] = null;
            
            if ($options['facebook']['active']) {
                include_once(WP_PLUGIN_DIR.'/'.plugin_basename(dirname(__FILE__)).'/src/facebook.php');
                
                $facebook = new Facebook(array(
                                               'appId'  => $options['facebook']['appId'],
                                               'secret' => $options['facebook']['secret'],
                                               'cookie' => true,
                                               ));
                $fbsession = $facebook->getUser();			
                $suararadio_connect['facebook']['obj'] = &$facebook;
                $suararadio_connect['facebook']['session']['uid'] = $fbsession;
                #var_dump($suararadio_connect);
                
            }
            if ($options['twitter']['active']) {
                include_once(WP_PLUGIN_DIR.'/'.plugin_basename(dirname(__FILE__)).'/src/twitter.image.php');
                //$twitter = new twitterImage('');
                //$suararadio_connect['twitter']['obj'] = &$twitter;
            }
 */
        }
} //endfunc suararadio_set_init
    
add_action('init', 'suararadio_set_init', 90);

function suararadio_get_fbuser() {
	global $suararadio_connect;

	if ($suararadio_connect['facebook']['obj'] && !$suararadio_connect['facebook']['user']) {
		try {
			$fbuser = $suararadio_connect['facebook']['obj']->api('/me');

		} catch (FacebookApiException $e) {
			$fbuser = null;
		}
		$suararadio_connect['facebook']['user'] = $fbuser;
	}
	return $suararadio_connect['facebook']['user'];
} //endfunc suararadio_get_fbuser

function suararadio_custom_login_head($clp_msg) {
	global $suararadio_connect;

	include(TEMPLATEPATH . '/header.php');
	echo '<div id="line_4" style="background:#fff;" align="center" style="float: left; clear: both;">'."\n";
	suararadio_fb_init_js();
}

function count_play($postId) {
	return podPress_playCount($postId);
}

function suararadio_new_user($user_id, $provider, $hybridauth_user_profile) {
	global $suararadio;
	global $arr_var_upd;

	// check apakah spin provider
	if ($provider=='Spin') {
		// tambahkan rutin untuk aktifkan melonnya.
		$suararadio->initAPI();
		
		// check ada member dengan SPIN tersebut ?
		#$params = array();
		#$params['Spin'] = $hybridauth_user_profile->identifier; 
		#$member = $suararadio->api->getMember($params);
		
		$user = get_userdata( $user_id );
		$vars = (array) $user->data;
		$vars['tipe'] = "20";
		$vars['vcode'] = 1; // 30 hari
		$vars['provider'] = "Spin";
		$vars['identity'] = $hybridauth_user_profile->identifier;
		$vars['regtime'] = $hybridauth_user_profile->datereg;
		$vars['exptime'] = $hybridauth_user_profile->exptime;
		
		$vars['home_url'] = defined('API_CLIENT_HOME_URL')? API_CLIENT_HOME_URL:get_option("siteurl");
		if (defined('IDRADIO')) $vars['radio_id'] = IDRADIO;
		
		$result = $suararadio->api->activate($vars);
		
		if ($result[code]=='1' || $result[code]=='2' || $result[code]=='3') {
			// update data local
			if (function_exists('update_user_meta')) {
				foreach ($arr_var_upd as $ky) {
					update_user_meta($user_id,$ky,$result[$ky]);
				}
			} else {
				foreach ($arr_var_upd as $ky) {
					update_usermeta($user_id,$ky,$result[$ky]);
				}
			}
		}
	}
	
	return $user_id;
} //endfunc suararadio_new_user

function suararadio_link_account($user_id, $provider, $hybridauth_user_profile) {
	global $suararadio;
	
	if (function_exists('update_user_meta')) {
		update_user_meta($user_id,$provider,$hybridauth_user_profile->identifier);
	} else {
		update_usermeta($user_id,$provider,$hybridauth_user_profile->identifier);
	}
	
	$user = get_userdata( $user_id );
	if ($user->klubid!='') {
		// update ke server
		$suararadio->initAPI();
		$suararadio->api->klubConnect($user->klubid,$provider,$hybridauth_user_profile->identifier);
	}
}//endfunc suararadio_link_account

add_action('wsl_hook_process_login_after_create_wp_user','suararadio_new_user',10,3);
add_action('wsl_hook_process_login_linked_account_before_redirect','suararadio_link_account',10,3);
